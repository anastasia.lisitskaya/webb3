<?php
//  Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
  $messages = array();
  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
      // Если есть параметр save, то выводим сообщение пользователю.
      ?><script> alert('Спасибо, данные были сохраненны.');</script>
      <?php
   
  }

// Складываем признак ошибок в массив.
$errors = array();
$errors['fio'] = !empty($_COOKIE['fio_error']);
$errors['email'] = !empty($_COOKIE['email_error']);
$errors['year'] = !empty($_COOKIE['year_error']);
$errors['radiogroup1'] = !empty($_COOKIE['radiogroup1_error']);
$errors['radiogroup2'] = !empty($_COOKIE['radiogroup2_error']);
$errors['abilities'] = !empty($_COOKIE['abilities_error']);
$errors['fieldname'] = !empty($_COOKIE['fieldname_error']);
$errors['checks'] = !empty($_COOKIE['checks_error']);


// Выдаем сообщения об ошибках.
if ($errors['fio']) {
  // Удаляем куку, указывая время устаревания в прошлом.
  setcookie('fio_error', '', 100000);
  // Выводим сообщение.
 if ($errors['fio'] == 'empty') $messages['fio'] = '<div class="errors">Name error.</div>';
 else $messages['fio'] = '<div class="errors">Only letters and white space allowed.</div>';
}
if ($errors['email']) {
    setcookie('email_error', '', 100000);
   if($errors['email'] == 'empty') $messages['email'] = '<div class="errors">Email is required.</div>';
   else $messages['email'] = '<div class="errors">Invalid email format.</div>';
  }
if ($errors['year']) {
    setcookie('year_error', '', 100000);
   $messages['year'] = '<div class="errors">Year is required.</div>';
  }
if ($errors['abilities']) {
    setcookie('abilities_error', '', 100000);
   $messages['abilities'] = '<div class="errors">Abilities is required.</div>';
  }
if ($errors['fieldname']) {
    setcookie('fieldname_error', '', 100000);
   $messages['fieldname'] = '<div class="errors">Fieldname is required.</div>';
  }

if ($errors['checks']) {
    setcookie('checks_error', '', 100000);
   $messages['checks'] = '<div class="errors">Checks is required.</div>';
  }

if ($errors['radiogroup1']) {
    setcookie('radiogroup1_error', '', 100000);
   $messages['radiogroup1'] = '<div class="errors">Radiogroup1 is required.</div>';
  }
if ($errors['radiogroup2']) {
    setcookie('radiogroup2_error', '', 100000);
   $messages['radiogroup2'] = '<div class="errors">Radiogroup2 is required.</div>';
  }

// Складываем предыдущие значения полей в массив, если есть.
$values = array();
$values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
$values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
$values['fieldname'] = empty($_COOKIE['fieldname_value']) ? '' : $_COOKIE['fieldname_value'];
$values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
$values['radiogroup1'] = empty($_COOKIE['radiogroup1_value']) ? '' : $_COOKIE['radiogroup1_value'];
$values['radiogroup2'] = empty($_COOKIE['radiogroup2_value']) ? '' : $_COOKIE['radiogroup2_value'];
$values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abiities_value'];
$values['checks'] = empty($_COOKIE['checks_value']) ? '' : $_COOKIE['checks_value'];

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else{
$errors = FALSE;

if (empty($_POST['fio'])) {
    // Выдаем куку до конца сессии с флажком об ошибке в поле fio.
    setcookie('fio_error', 'empty');
  $errors = TRUE;
}
else { if (!preg_match('/^[A-Za-zа-яА-Я\s]+$/iu',$_POST['fio'])) {
    setcookie('fio_error', 'invalid');
    $errors = TRUE;
       }
       else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
       }
    }

if (empty($_POST['email'])) {
    setcookie('email_error', 'empty');
    $errors = TRUE;
}
else { if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
    setcookie('fio_error', 'invalid');
    $errors = TRUE;
       }
       else {
    setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
       }
     }

if (empty($_POST['year'])) {
    setcookie('year_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('year_value', $_POST['year'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['radiogroup1']){
    setcookie('radiogroup1_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('radiogroup1_value', $_POST['radiogroup1'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['radiogroup2']){
    setcookie('radiogroup2_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('radiogroup2_value', $_POST['radiogroup2'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['abilities']){
    setcookie('abilities_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('abilities_value', $_POST['abilities'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['fieldname']){
    setcookie('fieldname_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('fieldname_value', $_POST['fieldname'], time() + 365 * 24 * 60 * 60);
  }


if (!$_POST['checks']){
    setcookie('checks_error', 'empty');
    $errors = TRUE;
}
else {
    setcookie('checks_value', $_POST['checks'], time() + 365 * 24 * 60 * 60);
  }



if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('radiogroup1_error', '', 100000);
    setcookie('radiogroup2', '', 100000);
     setcookie('abilities', '', 100000);
    setcookie('fieldname_error', '', 100000);
    setcookie('checks_error', '', 100000);
   
  }

  $ability1 = in_array('Bessmertie', $_POST['course']) ? 1 : 0;
  $ability2 = in_array('Prohozhdenie skvoz steni', $_POST['course']) ? 1 : 0;
  $ability3 = in_array('Levitation', $_POST['course']) ? 1 : 0;
  $ability4 = in_array('Chtenie misley', $_POST['course']) ? 1 : 0;
$ability5 = in_array('Hypersreed', $_POST['course']) ? 1 : 0;

  // Сохранение в базу данных 
  $user = 'u20418';
  $pass = '6489094';
  $db = new PDO('mysql:host=localhost;dbname=u20418', $user, $pass,
      array(PDO::ATTR_PERSISTENT => true));
  
  //Подготовленный запрос
  //неименованные метки
  
  try {
  $stmt = $db->prepare("INSERT INTO application  SET name = ?, email = ?, year = ? , radiogroup1 = ?, radiogroup2 = ?, abilities = ? , fieldname = ?, checks= ?  ");
  $stmt -> execute(array($_POST['fio'], $_POST['email'], $_POST['year'], $_POST['radiogroup1'], $_POST['radiogroup2'], $abilities, $_POST['fieldname'], $_POST['checks']));
}


  catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
  }
  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
